
#!/bin/bash

################################################### Configuração #####################################################

#1. Endereco para redmine

ADDRESS_REDMINE="localhost:9080/redmine"

#2. Endereco para gitlab

ADDRESS_GITLAB="192.168.1.113:80"

#3. dados de configuracao ldap

HOST="localhost"
PORT="389"
USERNAME="cn=admin,dc=sei,dc=com"
BASE="dc=sei,dc=com"
PASSWORD="sei1234"

#######################################################################################################################

cd /opt
git clone https://github.com/macartur/Ldap-Register.git 

cd Ldap-Register
bundle install
sudo yum install nodejs -y

# criar arquivo de configuração ldap
echo "host:           ${HOST}" > /opt/Ldap-Register/config/ldap.yml
echo "port:           ${PORT}     ">> /opt/Ldap-Register/config/ldap.yml
echo "userName:       ${USERNAME} " >> /opt/Ldap-Register/config/ldap.yml
echo "password:       ${PASSWORD} " >> /opt/Ldap-Register/config/ldap.yml 
echo "dc:             ${BASE} " >> /opt/Ldap-Register/config/ldap.yml

# criar arquivo de configuracao referencias

echo "redmine: ${ADDRESS_REDMINE}" > /opt/Ldap-Register/config/references.yml 
echo "gitlab:  ${ADDRESS_GITLAB} " >> /opt/Ldap-Register/config/references.yml 

sudo /usr/local/bin/unicorn_rails -c /opt/Ldap-Register/config/unicorn.rb -E production -l 0.0.0.0:443 -D
