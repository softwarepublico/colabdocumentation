#!/bin/bash

# Pré-requisitos

sudo yum update -y

# Instalação do Postgres 3

# Siga todas as instruções a seguir na máquina destinada ao banco de dados Postgres
# *NOTE:*
# 1. Libere a porta 5432 desta máquina para que máquina do colab possa ouvi-la

# 2. Instale o pacote postgresql

sudo yum localinstall http://yum.postgresql.org/9.3/redhat/rhel-6-x86_64/pgdg-centos93-9.3-1.noarch.rpm -y
sudo yum install postgresql93 postgresql93-devel postgresql93-libs postgresql93-server vim -y

# 3. Inicie o banco de dados

sudo service postgresql-9.3 initdb

# 4. Adicione o Postgres para iniciar com o sistema

sudo chkconfig postgresql-9.3 on

# 5. Inicie o postgresql

sudo service postgresql-9.3 start

# 6. Coloque os binários do postgres na variável PATH
echo "export PATH=$PATH:/usr/pgsql-9.3/bin/" >> ~/.bashrc
source ~/.bashrc
sudo sh -c "echo 'export PATH=$PATH:/usr/pgsql-9.3/bin/' >> ~/.bashrc"
sudo sh -c "source /root/.bashrc"

# 7. Edite o arquivo sudoers

sudo sed -i 's/\/sbin:\/bin:\/usr\/sbin:\/usr\/bin/\/sbin:\/bin:\/usr\/sbin:\/usr\/bin:\/usr\/pgsql-9.3\/bin/' /etc/sudoers

# 8. Crie todos os usuários e banco de dados necessários para o funcionamento correto do colab.

sudo -u postgres psql -c "CREATE ROLE redmine LOGIN ENCRYPTED PASSWORD 'redmine' NOINHERIT VALID UNTIL 'infinity';"
sudo -u postgres psql -c "CREATE DATABASE redmine WITH ENCODING='UTF8' OWNER=redmine;"

# 9. Altere o pg_hba.conf para conceder as permissões corretas aos usuários

sudo wget https://gitlab.com/softwarepublico/labsei/raw/master/arquivos/postgres/pg_hba.conf -O /var/lib/pgsql/9.3/data/pg_hba.conf

# 10. Reinicie o postgresql

sudo service postgresql-9.3 restart

